Rails.application.routes.draw do
  resources :posts
  root 'posts#index'

   # add this line to link tags to posts with the respective tag
  get 'tags/:tag', to: 'posts#index', as: :tag
end
